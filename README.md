This readme has gotten a little messy, so just text me if you're having trouble with anything.

Current app URL is 'newsapp.dev' (if using virutal host method of serving) but we can easily change later

Setup instructions:

	-Pull repo
	-run "composer install" in working directory to install dependencies (not sure if web server needs to already be running or not for this step)
	-Need to have minumum php version 1.7.3
	- ** Need to copy 'news' folder from repo to data folder of mysql installation (C:/xampp/mysql/data by default) **
	-Run 'php artisan passport:keys' in working directory (used to generate the keys necessary for generating API access keys)
------------------------------------------------------------------------------------------------------------------------------------------------	
	
	To use API:
	-Must have valid authorization token
	-To request one, make a POST request to newsapp.dev/oauth/token with the following body info:
		Key		  -  Value
		----------------------
		grant_type - password
		client_id  - 2												-Both client_id and client_secret can be hardcoded, will be the same for every token request
		client_secret - TjiOFWXCJWHTUNtoz1DpSR4AOw0ilscJSjWgbJeT
		username - <current user email>
		password - <current user password>
		scope - <leave blank>
		
	Token will be valid for 2 hours (same as default laravel session cookie lifetime). We shouldn't have to worry about
		refreshing it since we're only presenting for five minutes each. 
	Every request to api must include the access token by including the header 'Authorization' and the value of 'Bearer <access_token>'
	
	Example token request reply:
	{
    "token_type": "Bearer",
    "expires_in": 7199,
    "access_token": <long ass string>,
    "refresh_token": <slightly shorter long ass string>
	}
--------------------------------------------------------------------------------------------------------------------------------------------------
	
	How to run (3 choices)
		1. run 'php artisan serve' in working directory - use localhost:8000 
		2. create working directory within htdocs folder of web server - use localhost/<project_folder>/public
		3. (Best way) Create virtual host (accessible via newsapp.dev)
		
			-Edit C:/Windows/System32/drivers/etc/hosts (as admin)
				Add the following lines
				
				127.0.0.1 localhost
				127.0.0.1 newsapp.dev
			
			-Edit C:/xampp/apache/conf/extra/httpd-vhosts.conf
				Add the following lines
				
				<VirtualHost *:80>
    				DocumentRoot "C:/xampp/htdocs/"
   					 ServerName localhost
				</VirtualHost>

				<VirtualHost *:80>
    				DocumentRoot "C:/xampp/htdocs/426Final/public"
    				ServerName newsapp.dev
				</VirtualHost>
--------------------------------------------------------------------------------------------------------------------------------------
				
API paths: (Must include access_token for all)
~ = root page

	GET ~/api/sources - Source list
	GET ~/api/sources/<source_id> - Individual source
	GET ~/api/articles/<source_id> - All articles from source
	GET ~/api/articles/<source_id>/<article_id> - Single article (I might've forgotten to implement this one, but it seems a little redundant, let me know
																	if you need it though)
	
	GET ~/api/user_sources - Return list of user source selections
	POST ~/api/user_sources - Create user source selections
	PUT ~/api/user_sources - Update user source selections  - Must have x-www-form-urlencoded (instead of form-data) set for this to 
																work, can change to using a modified POST request instead if needed
	GET ~api/user - Returns current user info
	
	*** For PUT and POST on user_sources, include the actual selections under the key 'selections' in the body, list them as comma+space
		seperated values (ex. '1, 2, 3, 4' etc...)
		
	
Test user already made - email is test@test.com, password 123456, username Bob Newbie, feel free to make more ifneeded

Side note: 'long ass string' is my new favorite phrase