<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use GuzzleHttp\Client;
use App\Article;
use App\Source;

class ArticleController extends Controller
{
    public function indexSource($id) {
        $url = "https://newsapi.org/v2/top-headlines?";
        $apiKey = "59b34e358f3045e0970e5a38c02f53ac";

        $source = DB::table('sources')->where('id', $id);

        //Check to see if update needed 
        $time = new Carbon($source->value('updated_at'));
        $now = Carbon::now();
        $diff = $time->diffInHours($now, false);
        $numArticles = DB::table('articles')->where('source_id', '=', $id)->get();

        
        if ($diff > 0 or $numArticles->isEmpty() ) {
            $name = $source->value('url_name');

            $client = new Client();
            $res = $client->request('GET', $url . 'sources=' . $name . '&apiKey=' . $apiKey);

            DB::table('articles')->where('source_id', '=', $id)->delete();

            $articles = json_decode($res->getBody())->articles;
            foreach($articles as $article) {
                $datetime = str_replace(['T', 'Z'], " ", $article->publishedAt);
                $urlToImage = $article->urlToImage;
                if(strlen($urlToImage) > 190) {
                    $urlToImage = NULL;
                }
                $descrip = substr($article->description, 0, 1000);
                DB::table('articles')->insert(
                    ['author'=> $article->author, 'title' => $article->title,
                     'description' => $descrip, 'url' =>  $article->url,
                    'urlToImage' => $urlToImage, 'publishedAt' => $datetime, 'source_id' => $id]
                );
            }
            //Update source timestamp
            Source::find($id)->touch();
            
        }

        return DB::table('articles')->where('source_id', '=', $id)->get();
    }

    public function show($source_id, $id) {
        return DB::table('articles')->where('source_id', '=', $source_id)->where('id', '=', $id)->get();
    }

}