<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Source;

class SourceController extends Controller
{
    public function index() {
        return Source::all();
    }

    public function show(Source $source) {
        return $source;
    }
}
