<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use GuzzleHttp\Client;

class UserSourceController extends Controller
{
    public function getUserID($request) {
        $auth = $request->header('Authorization');
        $client = new Client();
        $res = $client->request('GET', 'newsapp.dev/api/user', [
            'headers' => [
                'Authorization' => $auth
            ]
        ]);
        $user_id = json_decode($res->getBody())->id;
        return $user_id;
    }

    public function show(Request $request) {
        $user_id = $this->getUserID($request);
        return DB::table('source_selections')->where('user_id', '=', $user_id)->select('source_id')->get();
    }

    public function make(Request $request) {
       
        $user_id = $this->getUserID($request);
        $sources = $request->input('selections');
        $sources = explode(", ", $sources);
        if (!DB::table('source_selections')->where('user_id', '=', $user_id)->exists()) {
            foreach($sources as $source) {
                DB::table('source_selections')->insert(
                    ['user_id' => $user_id,
                     'source_id' => $source
                     ]
                );
            }
        }
    }

    public function update(Request $request) {
        $user_id = $this->getUserID($request);
        DB::table('source_selections')->where('user_id', '=', $user_id)->delete();
        $sources = $request->input('selections');
        $sources = explode(", ", $sources);
        foreach($sources as $source) {
            DB::table('source_selections')->insert(
                ['user_id' => $user_id,
                 'source_id' => $source
                 ]
            );
        }  
        return "Success";
    }
}
