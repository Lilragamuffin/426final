-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 13, 2017 at 12:44 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `news`
--

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `id` int(10) UNSIGNED NOT NULL,
  `author` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `urlToImage` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `publishedAt` datetime NOT NULL,
  `source_id` int(10) UNSIGNED NOT NULL,
  `description` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id`, `author`, `title`, `url`, `urlToImage`, `publishedAt`, `source_id`, `description`) VALUES
(64, 'KEN THOMAS', 'Trump lashes out at own FBI in a series of tweets', 'https://apnews.com/81d263f198ae4ee18a8669d59c01d4e9', 'https://storage.googleapis.com/afs-prod/media/media:fd118c68c83a4485928ffe75bc8b9b8c/3000.jpeg', '2017-12-03 17:51:56', 2, 'WASHINGTON (AP) — President Donald Trump launched a fresh attack Sunday on the credibility of his own FBI, responding to revelations that an FBI agent was removed from special counsel Robert Mueller\'s team investigating Russian election meddling because of anti-Trump text messages. Trump, two days after his former national security adviser Michael Flynn pleaded guilty to lying to the FBI, again denied that he directed FBI Director James Comey to stop investigating Flynn.'),
(65, '', 'The Latest: Senator foresees obstruction of justice case', 'https://apnews.com/e5c75844a5c64a569fbbdb798992b9fb', 'https://storage.googleapis.com/afs-prod/media/media:8e90d3a1fc964234b403444c34066d9a/3000.jpeg', '2017-12-03 18:20:35', 2, 'WASHINGTON (AP) — The Latest on the removal of a veteran FBI agent from the special counsel\'s investigative team (all times local): 1:20 p.m. The top Democrat on the Senate Judiciary Committee says the panel is starting to see \"the putting together of a case of obstruction of justice\" against President Donald Trump. Speaking on NBC\'s \"Meet the Press,\" California Democrat Dianne Feinstein said the evidence is coming partly from \"the continual tweets\" from the White House. Trump is focusing renewed attacks on the FBI, and on the investigation by special counsel Robert Mueller, two days after ousted national security adviser Michael Flynn agreed to cooperate with the probe as part of a plea agreement. Feinstein said she believes Trump\'s firing of FBI Director James Comey came \"directly because he did not agree to lift the cloud of the Russia investigation.\" She added, \"That\'s obstruction of justice.\" Republican Sen. Lindsey Graham is warning Trump to tread cautiously, telling CBS\' \"Face'),
(66, 'MATTHEW LEE', 'Kushner: Trump still undecided on Israel\'s capital', 'https://apnews.com/c29eeb5691124e9aa948a627e4fa5012', 'https://storage.googleapis.com/afs-prod/media/media:bc2b4b2fe0c84adc941f9a56eb09e8aa/3000.jpeg', '2017-12-03 18:36:20', 2, 'WASHINGTON (AP) — President Donald Trump has not yet decided whether to recognize Jerusalem as Israel\'s capital or whether to proceed immediately in moving the U.S. Embassy from Tel Aviv to the holy city. That\'s according to his son-in-law and senior adviser Jared Kushner. Kushner said Sunday that the president continues to weigh his options ahead of an announcement on the matter that is expected this week. \"The president is going to make his decision,\" Kushner said in a rare public appearance at an event hosted by the Brookings Institution, a Washington think tank. \"He is still looking at a lot of different facts.\"'),
(67, 'AHMED AL-HAJ', 'Yemen\'s rebel alliance unravels amid Sanaa street clashes', 'https://apnews.com/c568c7b0c208440884143dcc2b496ad8', '', '2017-12-03 19:38:46', 2, 'SANAA, Yemen (AP) — Snipers took over rooftops in residential areas, tanks deployed and militiamen set up checkpoints Sunday across the Yemeni capital, where fighting forced families to hunker down indoors in anticipation of more violence. Five days of bombings and heavy gunfire have underscored the unraveling of the already fragile alliance between Yemen\'s strongman and former president, Ali Abdullah Saleh, and the Shiite rebels known as Houthis. The two sides joined ranks three years ago and swept across the capital, Sanaa, forcing the country\'s internationally recognized president to flee the country and seek military intervention led by Saudi Arabia.'),
(68, 'NICHOLAS RICCARDI', 'Utah\'s tenacious efforts to take back federal land stand out', 'https://apnews.com/74036ffce3a74ea7980c0754aa4c6798', 'https://storage.googleapis.com/afs-prod/media/media:6d2e57c780764eee8e9529fabea989c6/1992.jpeg', '2017-12-03 20:13:32', 2, 'DENVER (AP) — In an American West that has a love-hate relationship with the federal government\'s ownership of a checkerboard of parks, monuments, forest and desert in states from Washington to New Mexico, Utah stands out. The state has gone well beyond any other in the region in trying to pry the federal government\'s hands off land it sees as belonging to its residents. In 2012, its Legislature passed a law demanding the federal government give 30 million acres (121,000 square kilometers) of the land it owns in Utah to the state government — a measure other Western states have balked at replicating, even deeply conservative ones like Idaho.'),
(69, 'MARK SHERMAN', 'Some see echoes of \'68 court case in wedding cake dispute', 'https://apnews.com/b441f98193d84a0799aa9b322d70f752', 'https://storage.googleapis.com/afs-prod/media/media:6bde91a6e0234a3f863c69f03ca1fb64/3000.jpeg', '2017-12-03 16:23:22', 2, 'WASHINGTON (AP) — The upcoming Supreme Court argument about a baker who refused to make a cake for a same-sex couple makes some civil rights lawyers think of South Carolina\'s Piggie Park barbecue. When two African-Americans parked their car at a Piggie Park drive-in in August 1964 in Columbia, South Carolina, the waitress who came out to serve them turned back once she saw they were black and didn\'t take their order. In the civil rights lawsuit that followed, Piggie Park owner Maurice Bessinger justified the refusal to serve black customers based on his religious belief opposing \"any integration of the races whatsoever.\"'),
(70, 'GENE JOHNSON and DENISE LAVOIE', 'Fight over Trump travel restrictions back to appeals courts', 'https://apnews.com/69950362511b48458c759d7b69d60dc8', 'https://storage.googleapis.com/afs-prod/media/media:ba0e4c6dbccd489aa0befa1daa4bf8ac/3000.jpeg', '2017-12-03 18:24:02', 2, 'SEATTLE (AP) — For most of the time Syrian refugee Mohammad Al Zayed has been in the United States, judges have been wrestling with the Trump administration\'s efforts to impose travel restrictions that he says would keep him from seeing relatives who remain overseas. It\'s taken an emotional toll — one that continues this week as two U.S. appeals courts take up the issue yet again. \"It\'s been 10 months, and we\'re stuck,\" Al Zayed, a janitor at Chicago\'s O\'Hare International Airport, said through an Arabic interpreter. \"We can\'t go back. We can\'t bring our loved ones here.\"'),
(71, 'JULIET LINDERMAN', 'Amid the bloodshed, Baltimore group seeks to break the cycle', 'https://apnews.com/3ac32cf1dfe0466ca90a102b2be5b4f4', 'https://storage.googleapis.com/afs-prod/media/media:2d1856ef85b948c49bd0d586a26f5d37/3000.jpeg', '2017-12-03 15:41:48', 2, 'BALTIMORE (AP) — Akai Alston was 13 when he was shot for the first time. It was during a robbery in East Baltimore, in broad daylight. As he lay in his hospital bed, shaken and frightened, he knew he had a choice to make. \"I put it in my head that I\'d rather be a suspect than a victim,\" he said. Ten years later, Alston faced another grim decision. He was dealing crack and hooked on prescription pills. He\'d squeezed triggers, and seen friends and family members lose their lives to gunshots. After his last conviction, for accessory to murder, he knew was on the path to die in jail or die in the streets. This time, he rejected both.'),
(72, 'IVAN MORENO', 'US charter schools put growing numbers in racial isolation', 'https://apnews.com/e9c25534dfd44851a5e56bd57454b4f5', 'https://storage.googleapis.com/afs-prod/media/media:86e96dc48070417aacec5eb5eb13262c/3000.jpeg', '2017-12-03 10:37:32', 2, 'MILWAUKEE (AP) — Charter schools are among the nation\'s most segregated, an Associated Press analysis finds — an outcome at odds, critics say, with their goal of offering a better alternative to failing traditional public schools. National enrollment data shows that charters are vastly over-represented among schools where minorities study in the most extreme racial isolation. As of school year 2014-2015, more than 1,000 of the nation\'s 6,747 charter schools had minority enrollment of at least 99 percent, and the number has been rising steadily.'),
(73, 'RALPH D. RUSSO', 'Alabama joins Clemson, Oklahoma and Georgia in playoff', 'https://apnews.com/bfe589d1455d49ab836c7b0c9997676e', 'https://storage.googleapis.com/afs-prod/media/media:b07c3c585fb245cd84caf90735b89f2d/3000.jpeg', '2017-12-03 20:26:47', 2, 'Alabama is in, winning the great debate over Ohio State to reach the College Football Playoff yet again. The Crimson Tide got the nod Sunday for the fourth and final spot over Buckeyes and will play Clemson on Jan. 1 in the Sugar Bowl. Oklahoma and Georgia will meet in the Rose Bowl semifinal a few hours earlier. Tide or Buckeyes was the question facing the selection committee, the toughest call in the four-year history of the playoff.'),
(84, 'Julie Gerstein', '13 Eyebrow Trends From 2017 That Need To Go In The Trash', 'https://www.buzzfeed.com/juliegerstein/13-eyebrow-trends-from-2017-that-need-to-go-away-forever', 'https://img.buzzfeed.com/buzzfeed-static/static/2017-10/17/15/campaign_images/buzzfeed-prod-fastlane-03/xx-2017-eyebrow-trends-that-need-to-go-in-the-tra-2-12536-1508267686-2_dblbig.jpg', '2017-12-03 15:16:03', 4, 'That\'s a nope for me dawg.'),
(85, 'Andrew Ziegler', 'This \"On A Scale Of 1 To 10\" Quiz Will Determine If You\'re Type A Or Type B', 'https://www.buzzfeed.com/andrewziegler/on-a-scale-of-1-to-10', 'https://img.buzzfeed.com/buzzfeed-static/static/2017-11/28/18/campaign_images/buzzfeed-prod-fastlane-01/this-on-a-scale-of-1-to-10-quiz-will-determine-if-2-21364-1511910607-1_dblbig.jpg', '2017-12-03 20:46:02', 4, 'On a scale of 1 to 10...'),
(86, 'Morgan Shanahan', 'Julia Stiles Doesn\'t Give A Fuck What You Think Of Her Parenting Skills', 'https://www.buzzfeed.com/morganshanahan/julia-stiles-doesnt-give-a-fuck-what-you-think-of-her', 'https://img.buzzfeed.com/buzzfeed-static/static/2017-12/3/14/campaign_images/buzzfeed-prod-fastlane-03/julia-stiles-doesnt-give-a-fuck-what-you-think-of-2-1793-1512330811-7_dblbig.jpg', '2017-12-03 19:53:34', 4, '\"That\'s the internet for you, the carnivorous plant from \'Little Shop of Horrors\'.\"'),
(87, 'Ishmael N. Daro', 'Rogers Fired Sporstnet Commentator Gregg Zaun For \"Inappropriate Behaviour\"', 'https://www.buzzfeed.com/ishmaeldaro/gregg-zaun-fired', 'https://img.buzzfeed.com/buzzfeed-static/static/2017-12/1/12/campaign_images/buzzfeed-prod-fastlane-03/rogers-fired-sporstnet-commentator-gregg-zaun-for-2-27563-1512149120-1_dblbig.jpg', '2017-12-01 17:25:26', 4, 'His termination is effective immediately, the company said.'),
(88, 'Ryan Schocket', '15 Things From This Week That Will Make You Sound Smart AF', 'https://www.buzzfeed.com/ryanschocket2/16-things-from-this-week-that-will-make-you-sound-smart-af', 'https://img.buzzfeed.com/buzzfeed-static/static/2017-12/3/15/campaign_images/buzzfeed-prod-fastlane-03/15-things-from-this-week-that-will-make-you-sound-2-3822-1512331426-0_dblbig.jpg', '2017-12-03 20:03:49', 4, 'Cinnamon helps you lose weight??..??????....???'),
(89, 'Jon-Michael Poff', '19 Pop Stars When Their Debut Album Came Out Vs. Now', 'https://www.buzzfeed.com/jonmichaelpoff/pop-stars-when-their-debut-album-came-out-vs-now', 'https://img.buzzfeed.com/buzzfeed-static/static/2017-11/30/14/tmp/buzzfeed-prod-fastlane-03/4a0f01c8debae73ae5596762c0a1cb4c-8.jpg?crop=625:327;0,17', '2017-12-03 19:01:02', 4, 'Time flies!'),
(90, 'Stephen LaConte', 'Beyoncé Changes Her Email Address Every Single Week Because Of Course She Does', 'https://www.buzzfeed.com/stephenlaconte/beyonce-changes-her-email-address-every-single-week-because', 'https://img.buzzfeed.com/buzzfeed-static/static/2017-12/3/14/campaign_images/buzzfeed-prod-fastlane-03/beyonce-changes-her-email-address-every-single-we-2-2883-1512330352-0_dblbig.jpg', '2017-12-03 19:45:56', 4, 'But her emails.'),
(91, 'Lauren Strapagiel', 'The Quebec Government Doesn\'t Want Shop Employees To Say \"Hi\" Anymore', 'https://www.buzzfeed.com/laurenstrapagiel/the-quebec-government-doesnt-want-shop-employees-to-say-hi', 'https://img.buzzfeed.com/buzzfeed-static/static/2017-11/30/17/campaign_images/buzzfeed-prod-fastlane-01/the-quebec-government-doesnt-want-shop-employees--2-23150-1512080473-0_dblbig.jpg', '2017-11-30 22:12:48', 4, 'O hai.'),
(92, 'Chris Peña', 'Rate These Ice Cream Flavors And We\'ll Tell You Your Best Quality', 'https://www.buzzfeed.com/christopherpena/rate-these-ice-cream-flavors-and-well-tell-you-your-best', 'https://img.buzzfeed.com/buzzfeed-static/static/2017-11/29/16/campaign_images/buzzfeed-prod-fastlane-01/rate-these-ice-cream-flavors-and-well-tell-you-yo-2-14251-1511991160-15_dblbig.jpg', '2017-12-03 19:46:02', 4, 'It\'s a win-win!'),
(93, 'Ryan Schocket', 'Pink Opened Up About Raising Her Daughter Gender Neutral', 'https://www.buzzfeed.com/ryanschocket2/pink-opened-up-about-raising-her-daughter-gender-neutral', 'https://img.buzzfeed.com/buzzfeed-static/static/2017-12/3/12/campaign_images/buzzfeed-prod-fastlane-03/pink-opened-up-about-raising-her-daughter-gender--2-32176-1512323671-0_dblbig.jpg', '2017-12-03 17:54:34', 4, 'No labels, no problem.'),
(94, 'BBC News', 'Trump lashes out at FBI over Russia probe', 'http://www.bbc.co.uk/news/world-us-canada-42215767', 'https://ichef.bbci.co.uk/news/1024/cpsprodpb/141D9/production/_99039328_mediaitem99039327.jpg', '2017-12-03 15:28:26', 3, 'In a Twitter tirade, the president issues a fresh denial that he tried to obstruct FBI investigation.'),
(95, 'BBC News', 'Afghan leader apologises to women', 'http://www.bbc.co.uk/news/world-asia-42215547', 'https://ichef-1.bbci.co.uk/news/1024/cpsprodpb/131D5/production/_99039287_gettyimages-672230608.jpg', '2017-12-03 14:45:17', 3, 'Ashraf Ghani says his comments about the traditional headscarf have been misinterpreted.'),
(96, 'BBC News', 'Christmas market bomb \'was extortion bid\'', 'http://www.bbc.co.uk/news/world-europe-42214932', 'https://ichef.bbci.co.uk/news/1024/cpsprodpb/2FA1/production/_99039121_polizei.jpg', '2017-12-03 18:00:04', 3, 'A bomb found at a Christmas market in Germany on Friday was an attempt to extort from DHL.'),
(97, 'BBC News', 'Moment US stadium demolition fails', 'http://www.bbc.co.uk/news/world-us-canada-42216297', 'https://ichef.bbci.co.uk/news/1024/cpsprodpb/F680/production/_99040136_p05pywgf.jpg', '2017-12-03 15:49:28', 3, 'Detroit\'s Pontiac Silverdome stadium is still standing after Sunday\'s planned demolition didn\'t go to plan.'),
(98, 'BBC Sport', 'India v Sri Lanka Test match halted by smog in Delhi', 'http://www.bbc.co.uk/sport/cricket/42213491', 'https://ichef.bbci.co.uk/onesport/composite-logo/3AE1/production/_99037051_smog.jpg', '2017-12-03 10:05:08', 3, 'Sri Lanka\'s players take to wearing masks to combat the smog pollution disrupting the third Test against India in Delhi.'),
(99, 'BBC News', '\'No desire\' in Ireland to delay Brexit', 'http://www.bbc.co.uk/news/world-europe-42214313', 'https://ichef-1.bbci.co.uk/news/1024/cpsprodpb/1315D/production/_99037187_hi043299116.jpg', '2017-12-03 10:32:24', 3, 'Deputy PM says Ireland is \"uniquely vulnerable\" and needs to see more progress on the border.'),
(100, 'BBC News', 'Deported Egypt politician \'disappears\'', 'http://www.bbc.co.uk/news/world-middle-east-42216147', 'https://ichef-1.bbci.co.uk/news/1024/cpsprodpb/12CCC/production/_99040077_hi043281843.jpg', '2017-12-03 16:24:53', 3, 'A former Egyptian PM\'s family say they do not know where he is, following his deportation from UAE.'),
(101, 'BBC News', 'ABC suspends reporter over Flynn report', 'http://www.bbc.co.uk/news/world-us-canada-42214214', 'https://ichef.bbci.co.uk/news/1024/cpsprodpb/E3FB/production/_99036385_mediaitem99036384.jpg', '2017-12-03 11:53:40', 3, 'An erroneous ABC report on Michael Flynn and the Russia inquiry sent stock market shares tumbling.'),
(102, 'BBC News', 'US \'in race to address N Korea threat\'', 'http://www.bbc.co.uk/news/world-us-canada-42213541', 'https://ichef.bbci.co.uk/news/1024/cpsprodpb/12E33/production/_99036377_146bf39b-a962-4089-83bc-d52c12468037.jpg', '2017-12-03 09:22:58', 3, 'National security adviser HR McMaster says North Korea is the \"greatest immediate threat to the US\".'),
(103, 'BBC News', 'Thirteen dead in S Korea boat crash', 'http://www.bbc.co.uk/news/world-asia-42213513', 'https://ichef-1.bbci.co.uk/news/1024/cpsprodpb/115FB/production/_99036117_hi043356954.jpg', '2017-12-03 07:50:32', 3, 'A fishing boat overturns after colliding with a 336-tonne tanker off the South Korean coast.'),
(126, 'ABC News', 'Trump has \'no appreciation for diplomacy\': Senate Democrat', 'http://abcnews.go.com/Politics/trump-appreciation-diplomacy-world-senate-democrat/story?id=51692437', 'http://a.abcnews.com/images/Politics/ben-cardin-2-gty-jt-171209_16x9_992.jpg', '2017-12-10 00:00:00', 1, 'The top Democrat on the Senate Foreign Relations Committee said he agreed with President Donald Trump’s decision to recognize Jerusalem as Israel\'s capital but that the announcement should have been handled with greater diplomacy and as a way to advance the Middle East peace process.\n\"We’ve seen...'),
(127, 'ABC News', 'Puerto Rico\'s Maria death toll climbs to 64, as FEMA assistance tops $1 billion', 'http://abcnews.go.com/US/puerto-ricos-hurricane-maria-death-toll-climbs-64/story?id=51698527', 'http://a.abcnews.com/images/US/puerto-rico-hurricane-gty-ml-171020_16x9_992.jpg', '2017-12-10 00:00:00', 1, 'Puerto Rico\'s death toll from Hurricane Maria -- which slammed into the U.S. territory September 20 as a Category 4 storm -- has risen to 64, the island\'s Department of Public Safety (DPS) announced Saturday.\nThe announcement came the same day that the Federal Emergency Management Agency said...'),
(128, 'ABC News', 'Most Alabama Republicans say they are voting for Roy Moore', 'http://abcnews.go.com/Politics/wireStory/alabama-republicans-voting-roy-moore-51698664', 'http://a.abcnews.com/images/Politics/WireAP_39d604ad148140c1b78d01e70b861ff1_16x9_992.jpg', '2017-12-10 00:00:00', 1, 'Most Republicans leaders in Alabama say they plan to vote for Roy Moore on Tuesday, despite sexual misconduct allegations against the former judge that have prompted others around the country to say he should never be allowed to join the U.S. Senate. \"I have stated both publicly and...'),
(129, 'ABC News', 'Women say sexual misconduct common in hospitality industry', 'http://abcnews.go.com/US/wireStory/hospitality-industry-sexual-misconduct-part-job-51701302', 'http://a.abcnews.com/images/US/WireAP_d01dc2e6f410402c8109ae0a085a56f2_16x9_992.jpg', '2017-12-10 00:00:00', 1, 'One woman recalls how a general manager at a Chicago-area restaurant where she worked told her that if security cameras recorded him reaching between her legs and grabbing her genitals he could simply \"edit that out.\" Another woman worked at an Atlanta restaurant and says her boss did...'),
(130, 'ABC News', 'Winter storm bids farewell to the snow-covered Northeast, blast of cold air is next', 'http://abcnews.go.com/US/winter-storm-bids-farewell-snow-covered-northeast-blast/story?id=51698902', 'http://a.abcnews.com/images/US/WireAP_afec536900ed47b09df6413caad67874_16x9_992.jpg', '2017-12-10 00:00:00', 1, 'So long, snow.\nAfter a winter storm dumped the white stuff on parts of the Gulf Coast and Southeast on Friday, the mid-Atlantic, Northeast and New England were hit with heavy snow on Saturday. But as of Sunday morning, the storm is leaving the Northeast.\nFinal snow totals in northern Georgia...'),
(131, 'ABC News', 'Arab ministers demand reversal of Trump Jerusalem decision', 'http://abcnews.go.com/International/wireStory/arab-foreign-ministers-meet-cairo-jerusalem-51691102', 'http://a.abcnews.com/images/International/WireAP_ccf80762139845749438467950262402_16x9_992.jpg', '2017-12-09 00:00:00', 1, 'Arab foreign ministers on Sunday demanded that the United States rescind President Donald Trump\'s decision to recognize Jerusalem as Israel\'s capital, calling it a \"grave\" development that puts Washington on the same side as \"occupation\" and the violation of international law. In a...'),
(132, 'ABC News', '3 people arrested in firebomb attack on Swedish synagogue', 'http://abcnews.go.com/International/wireStory/people-arrested-fire-bomb-attack-swedish-synagogue-51698899', 'http://a.abcnews.com/images/International/WireAP_2b2c9065fc2044bb97eb81172c968c76_16x9_992.jpg', '2017-12-10 00:00:00', 1, 'Three people have been arrested for allegedly throwing firebombs at a synagogue in the Swedish city of Goteborg, the second anti-Jewish attack in the Nordic nation in two days. Jewish groups condemned the attacks as \"unconscionable\" and demanded that authorities take action. No one was...'),
(133, 'ABC News', 'Spain rescues 104 migrants crossing Mediterranean Sea', 'http://abcnews.go.com/International/wireStory/spain-rescues-104-migrants-crossing-mediterranean-sea-51699569', NULL, '2017-12-10 00:00:00', 1, 'Spain\'s maritime rescue service says it has saved 104 migrants trying to make the perilous crossing of the Mediterranean Sea to Europe from North Africa. The service says its rescue craft Guardamar Concepcion Arenal intercepted two boats carrying 53 and 22 migrants each overnight and early...'),
(134, 'ABC News', 'Police link 4th freeway shooting to same suspects in Detroit', 'http://abcnews.go.com/US/wireStory/police-link-4th-freeway-shooting-suspects-detroit-51703466', 'http://a.abcnews.com/images/US/ho-michigan-police-highway-mo-20171208_16x9_992.jpg', '2017-12-10 00:00:00', 1, 'Michigan State Police officials say they now believe four shootings on Detroit\'s freeway system are linked to the same suspect or suspects. The agency\'s Metro Detroit post says a woman recently reported that her car was hit by gunfire on Thursday along Interstate 94 on the city\'s east side....');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`) VALUES
(1, 'Business'),
(2, 'Entertainment'),
(3, 'Gaming'),
(4, 'General'),
(5, 'Health'),
(6, 'Music'),
(7, 'Politics'),
(8, 'Science and Nature'),
(9, 'Sport'),
(10, 'Technology');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(17, '2014_10_12_000000_create_users_table', 1),
(18, '2014_10_12_100000_create_password_resets_table', 1),
(19, '2017_12_03_162218_create_sources_table', 1),
(20, '2017_12_03_162245_create_articles_table', 1),
(23, '2017_12_03_201132_drop_articles_timestamps', 2),
(24, '2017_12_03_204912_drop_description_column_articles', 2),
(25, '2017_12_03_205228_add_description_as_string', 3),
(26, '2016_06_01_000001_create_oauth_auth_codes_table', 4),
(27, '2016_06_01_000002_create_oauth_access_tokens_table', 4),
(28, '2016_06_01_000003_create_oauth_refresh_tokens_table', 4),
(29, '2016_06_01_000004_create_oauth_clients_table', 4),
(30, '2016_06_01_000005_create_oauth_personal_access_clients_table', 4),
(31, '2017_12_10_172034_add_source_selections', 5),
(35, '2017_12_11_045240_create_category_table', 6);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Epic News App Personal Access Client', 'cZ8BTJKxTpd2UWbrm3uAMK15OyGWPreA09MTkh8U', 'http://localhost', 1, 0, 0, '2017-12-08 06:54:55', '2017-12-08 06:54:55'),
(2, NULL, 'Epic News App Password Grant Client', 'TjiOFWXCJWHTUNtoz1DpSR4AOw0ilscJSjWgbJeT', 'http://localhost', 0, 1, 0, '2017-12-08 06:54:55', '2017-12-08 06:54:55'),
(3, NULL, 'News', 'IIY3xawGp70m0ZCs5ieDiwYcfuWmYZIcpHjj2flz', 'http://newsapp.dev/home', 0, 0, 0, '2017-12-10 21:36:01', '2017-12-10 21:36:01');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2017-12-08 06:54:55', '2017-12-08 06:54:55');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sources`
--

CREATE TABLE `sources` (
  `id` int(10) UNSIGNED NOT NULL,
  `url_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo_loc` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `category_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sources`
--

INSERT INTO `sources` (`id`, `url_name`, `name`, `logo_loc`, `created_at`, `updated_at`, `category_id`) VALUES
(1, 'abc-news', 'ABC News', 'images/logos/abc.png', '2017-12-03 21:59:21', '2017-12-11 06:34:21', 4),
(2, 'associated-press', 'Associated Press', 'images/logs/apnews.png', '2017-12-03 22:01:00', '2017-12-04 01:58:54', 4),
(3, 'bbc-news', 'BBC News', 'images/logos/bbc.png', '2017-12-03 22:01:27', '2017-12-04 02:02:05', 4),
(4, 'buzzfeed', 'Buzzfeed', 'images/logos/buzzfeed.png', '2017-12-03 22:02:16', '2017-12-04 02:00:22', 2),
(5, 'cnn', 'CNN', 'images/logos/cnn.png', '2017-12-03 22:02:41', '2017-12-03 22:02:41', 4),
(6, 'entertainment-weekly', 'Entertainment Weekly', 'images/logos/entertainment_weekly.png', '2017-12-06 01:14:47', '2017-12-06 01:14:47', 2),
(7, 'espn', 'ESPN', 'images/logos/espn.png', '2017-12-06 01:15:48', '2017-12-06 01:15:48', 9),
(8, 'fox-news', 'Fox News', 'images/logos/foxnews.png', '2017-12-06 01:16:41', '2017-12-06 01:16:41', 4),
(9, 'fox-sports', 'Fox Sports', 'images/logos/foxsports.png', '2017-12-06 01:17:55', '2017-12-06 01:17:55', 9),
(10, 'google-news', 'Google News', 'images/logo/googlenews.png', '2017-12-06 01:18:47', '2017-12-06 01:18:47', 4),
(11, 'ign', 'IGN', 'images/logos/ign.png', '2017-12-06 01:19:18', '2017-12-06 01:19:18', 3),
(12, 'msnbc', 'MSNBC', 'images/logos/msnbc.png', '2017-12-06 01:19:46', '2017-12-06 01:19:46', 4),
(13, 'national-geographic', 'National Geographic', 'images/logos/natgeo.png', '2017-12-06 01:20:14', '2017-12-06 01:20:14', 8),
(14, 'nbc-news', 'NBC News', 'images/logos/nbc.png', '2017-12-06 01:20:49', '2017-12-06 01:20:49', 4),
(15, 'reuters', 'Reuters', 'images/logos/reuters.png', '2017-12-06 01:21:17', '2017-12-06 01:21:17', 4),
(16, 'techcrunch', 'TechCrunch', 'images/logos/techcrunch.png', '2017-12-06 01:21:51', '2017-12-06 01:21:51', 10),
(17, 'the-huffington-post', 'The Huffington Post', 'images/logos/huffington_post.png', '2017-12-06 01:22:28', '2017-12-06 01:22:28', 4),
(18, 'the-new-york-times', 'The New York Times', 'images/logos/tnytimes.png', '2017-12-06 01:22:59', '2017-12-06 01:22:59', 4),
(19, 'the-verge', 'The Verge', 'images/logos/theverge.png', '2017-12-06 01:23:22', '2017-12-06 01:23:22', 10),
(20, 'the-wall-street-journal', 'The Wall Street Journal', 'images/logos/thewsj.png', '2017-12-06 01:23:53', '2017-12-06 01:23:53', 1),
(21, 'the-washington-post', 'The Washington Post', 'images/logos/thewp.png', '2017-12-06 01:24:14', '2017-12-06 01:24:14', 4),
(22, 'time', 'Time', 'images/logos/time.png', '2017-12-06 01:24:31', '2017-12-06 01:24:31', 4),
(23, 'usa-today', 'USA Today', 'images/logos/usatoday.png', '2017-12-06 01:24:53', '2017-12-06 01:24:53', 4),
(24, 'wired', 'Wired', 'images/logos/wired.png', '2017-12-06 01:25:11', '2017-12-06 01:25:11', 10),
(25, 'argaam', 'Argaam', 'images/logos/argaam.png', '2017-12-11 10:39:51', '2017-12-11 10:39:51', 1),
(26, 'bloomberg', 'Bloomberg', 'images/logos/bloomberg.png', '2017-12-11 10:41:26', '2017-12-11 10:41:26', 1),
(27, 'business-insider', 'Business Insider', 'images/logos/businessinsider.png', '2017-12-11 10:41:59', '2017-12-11 10:41:59', 1),
(28, 'cnbc', 'CNBC', 'images/logos/cnbc.png', '2017-12-11 10:42:38', '2017-12-11 10:42:38', 1),
(29, 'die-zeit', 'Die Zeit', 'images/logos/diezeit.png', '2017-12-11 10:43:08', '2017-12-11 10:43:08', 1),
(30, 'financial-post', 'Financial Post', 'images/logos/financial-post.png', '2017-12-11 10:43:44', '2017-12-11 10:43:44', 1),
(31, 'financial-times', 'Financial Times', 'images/logos/financialtimes.png', '2017-12-11 10:44:13', '2017-12-11 10:44:13', 1),
(32, 'fortune', 'Fortune', 'images/logos/fortune.png', '2017-12-11 10:44:35', '2017-12-11 10:44:35', 1),
(33, 'handelsblatt', 'Handelsblatt', 'images/logos/handelsblatt.png', '2017-12-11 10:45:03', '2017-12-11 10:45:03', 1),
(34, 'info-money', 'InfoMoney', 'images/logos/infomoney.png', '2017-12-11 10:45:30', '2017-12-11 10:45:30', 1),
(35, 'les-echos', 'Les Echos', 'images/logos/lesechos.png', '2017-12-11 10:45:55', '2017-12-11 10:45:55', 1),
(36, 'the-economist', 'The Economist', 'images/logos/economist.png', '2017-12-11 10:46:23', '2017-12-11 10:46:23', 1),
(37, 'daily-mail', 'Daily Mail', 'images/logos/dailymail.png', '2017-12-11 10:54:18', '2017-12-11 10:54:18', 2),
(38, 'mashable', 'Mashable', 'images/logos/mashable.png', '2017-12-11 10:54:40', '2017-12-11 10:54:40', 2),
(39, 'the-lad-bible', 'The Lad Bible', 'images/logos/ladbible.png', '2017-12-11 10:55:04', '2017-12-11 10:55:04', 2),
(40, 'polygon', 'Polygon', 'images/logos/polygon.png', '2017-12-11 10:56:48', '2017-12-11 10:56:48', 3),
(41, 'medical-news-today', 'Medical News Today', 'images/logos/mednewstoday.png', '2017-12-11 10:58:42', '2017-12-11 10:58:42', 5),
(42, 'mtv-news', 'MTV News', 'images/logos/mtvnews.png', '2017-12-11 10:59:39', '2017-12-11 10:59:39', 6),
(43, 'breitbart-news', 'Breitbart News', 'images/logos/breitbart.png', '2017-12-11 11:00:26', '2017-12-11 11:00:26', 7),
(44, 'politico', 'Politico', 'images/logos/politico.png', '2017-12-11 11:00:58', '2017-12-11 11:00:58', 7),
(45, 'the-hill', 'The Hill', 'images/logos/thehill.png', '2017-12-11 11:01:22', '2017-12-11 11:01:22', 7),
(46, 'new-scientist', 'New Scientist', 'images/logos/newscientist.png', '2017-12-11 11:02:35', '2017-12-11 11:02:35', 8),
(47, 'next-big-future', 'Next Big Future', 'images/logos/nextbigfuture.png', '2017-12-11 11:03:14', '2017-12-11 11:03:14', 8);

-- --------------------------------------------------------

--
-- Table structure for table `source_selections`
--

CREATE TABLE `source_selections` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `source_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `source_selections`
--

INSERT INTO `source_selections` (`user_id`, `source_id`) VALUES
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(1, 6);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Bob Newbie', 'test@test.com', '$2y$10$LKMR3.41bYrpf.pJQu77vOgs2TbzcG0Yo3dsezcYTsyWxT0sWZIhG', 'sSyoeli3KNMdlOxUxZ5VCtMGQlZghy9zZXbyGb6RJ78iwzJpz4zryPTOFOSo', '2017-12-10 21:32:16', '2017-12-10 21:32:16');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `articles_source_id_foreign` (`source_id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `sources`
--
ALTER TABLE `sources`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `source_selections`
--
ALTER TABLE `source_selections`
  ADD PRIMARY KEY (`user_id`,`source_id`),
  ADD KEY `source_selections_source_id_foreign` (`source_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=135;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sources`
--
ALTER TABLE `sources`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `articles`
--
ALTER TABLE `articles`
  ADD CONSTRAINT `articles_source_id_foreign` FOREIGN KEY (`source_id`) REFERENCES `sources` (`id`);

--
-- Constraints for table `sources`
--
ALTER TABLE `sources`
  ADD CONSTRAINT `sources_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`);

--
-- Constraints for table `source_selections`
--
ALTER TABLE `source_selections`
  ADD CONSTRAINT `source_selections_source_id_foreign` FOREIGN KEY (`source_id`) REFERENCES `sources` (`id`),
  ADD CONSTRAINT `source_selections_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
