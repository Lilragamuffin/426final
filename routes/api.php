<?php

use Illuminate\Http\Request;
use App\Source;
use App\Article;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('sources', 'SourceController@index');

Route::get('sources/{source}', 'SourceController@show');

Route::get('articles/{source_id}', 'ArticleController@indexSource');

Route::get('articles/{source_id}/{id}', 'ArticleController@show');

Route::post('user_sources', 'UserSourceController@make');

Route::get('user_sources', 'UserSourceController@show');

Route::put('user_sources', 'UserSourceController@update');

Route::group(['prefix' => 'user'], function() {
    Route::get('/', function() {
        return response()->json(request()->user());
    });
});
